mc.setProperty('CONTENT_TYPE', 'application/json');
var request = mc.getProperty('uri.var.request');

var organization = {
    id:1,
    address: 'dummy address',
    name: 'dummy name',
    departments:[
      {
        id:1,
        idOrganization: 1,
        name:'department 1'
      },
      {
        id:2,
        idOrganization: 1,
        name:'department 2'
      }
    ],
    employees:[
      {
        id: 1,
        idOrganization: 1,
        idDepartment: 1,
        name: 'dummy employee 1',
        position: 'dummy position 1',
        age: 21
      },
      {
        id: 2,
        idOrganization: 1,
        idDepartment: 2,
        name: 'dummy employee 2',
        position: 'dummy position 2',
        age: 22
      }
    ]
};

mc.setPayloadJSON(JSON.stringify(organization));
