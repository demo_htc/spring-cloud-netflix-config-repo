

CREATE TABLE organization(
	id serial PRIMARY KEY NOT NULL,
	name varchar(60) NOT NULL,
	address varchar(50) NOT NULL
);

CREATE TABLE department(
	id serial PRIMARY KEY NOT NULL,
	id_organization int REFERENCES organization (id) NOT NULL,
	name varchar(50) NOT NULL
);

CREATE TABLE employee(
	id serial PRIMARY KEY NOT NULL,
	id_organization int REFERENCES organization (id) NOT NULL,
	id_department int REFERENCES department (id) NOT NULL,
	name varchar(50) NOT NULL,
	age int NOT NULL,
	position varchar(60) NOT NULL
);

INSERT INTO public.organization
("name", address)
VALUES('High Tech Consulting - Central America', 'El Salvador'),('High Tech Consulting - South America', 'Bolivia'),('High Tech Consulting - University', 'El Salvador');

select * from organization;

INSERT INTO public.department
(id_organization, "name")
VALUES(1, 'Software Factory'), (2, 'Quality Assurance'),(3, 'Database Administrator');

select * from department;

INSERT INTO public.employee
(id_organization, id_department, "name", age, "position")
VALUES(1, 1, 'Manfredo', 21, 'Chief Oracle OSB Architec'),(2, 2, 'Gerardo', 25, 'Chief Selenium QA Tester'),(3, 3, 'Luis', 32, 'Chief Oracle DB Admin');

select * from employee;

